import React, {useEffect, useState} from 'react';
import '../styles/Slider.css'

const Slider = (props) => {

    const {picFolder, pictures} = props;

    const [activeSlide, setActiveSlide] = useState(0)

    const changeSlide = () => {
        if (activeSlide === pictures.length - 1) {
            setActiveSlide(0)
        } else {
            setActiveSlide(activeSlide + 1)
        }
    }
     useEffect(() => {
         const sliderInterval = setInterval(changeSlide, 3000)
         return () => clearInterval(sliderInterval)
         }
     )

    const Slide = ({ children, isActive, slide, onClick }) => {
        let className = 'slide'
        if (isActive) {
            className += ' active';
        }
        return (
            <div
                onClick={onClick}
                className={className}
                style={{'background': 'url('+picFolder + '/' + slide + ') no-repeat', 'backgroundSize': 'contain', 'backgroundPosition': 'center'}}
            >
                {children}
            </div>
        );
    }

    const handleClick = (event, index) => {
        if(index === activeSlide) {
            const element = event.target.getBoundingClientRect()
            const mousePosX = event.clientX - element.left
            const percent = mousePosX / element.width * 100
            console.log(percent)
            if(percent < 50) {
                console.log('left')
                if(activeSlide === 0) {
                    setActiveSlide(pictures.length - 1)
                } else {
                    setActiveSlide(activeSlide - 1)
                }
            } else {
                console.log('right')
                if(activeSlide === pictures.length - 1) {
                    setActiveSlide(0)
                } else {
                    setActiveSlide(activeSlide + 1)
                }
            }
        }
    }

    const slides = pictures.map((slide, index) => (
            <Slide
                onClick={(event) => handleClick(event, index)}
                key={slide}
                slide={slide}
                isActive={index === activeSlide}
            >
            </Slide>
        )
    )

    return (
        <div>
            {slides}
        </div>
    );
};

export default Slider;