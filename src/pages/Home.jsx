import React from 'react';
import Slider from '../components/Slider'

const Home = () => {

    const picFolder = 'pictures';
    const pictures = ['1.jpg', '2.jpg', '3.jpg', '4.jpg']

    return (
        <>
            <Slider picFolder = {picFolder} pictures = {pictures} />
        </>
    );
};

export default Home;